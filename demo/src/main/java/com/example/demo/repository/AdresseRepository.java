package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Adresse;

public interface AdresseRepository extends JpaRepository<Adresse, Integer> {
    
}
