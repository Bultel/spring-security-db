package com.example.demo.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class Adresse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @NonNull
    String rue;

    @NonNull
    String codePostale;

    @NonNull
    String ville;

    //@NonNull
    //Relation bidirectionnel - adresses fait ref au adresses de personne
    //Pas d'ordre pour la relation
    @ManyToMany(mappedBy = "adresses")
    //evite les boucles de données
    @JsonIgnoreProperties(value = "personnes")
    List<Personne> personnes;

}
