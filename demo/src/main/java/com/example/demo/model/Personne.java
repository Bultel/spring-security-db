package com.example.demo.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor

public class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer num;

    @NonNull
    String nom;

    @NonNull
    String prenom;


    //QUAnd je perciste personne, je persiste aussi les adresses pas besoin de MAPEDBY quand la relation n'est pas bidirectionnel
    //@ManyToMany(cascade = CascadeType.PERSIST)

    //Persmet de ajouter et modifier une liste d'adresse
    @ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @NonNull
    //evite les boucles de données
    @JsonIgnoreProperties(value = "personnes")
    List<Adresse> adresses;
}
