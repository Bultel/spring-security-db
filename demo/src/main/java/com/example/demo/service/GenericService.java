package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Personne;

public interface GenericService {

    public List<Personne> findAll();
    public Personne findById(int id);
    public Personne save(Personne personne);
    public Personne update(Personne personne);
    public void deleteById(int id);
}
