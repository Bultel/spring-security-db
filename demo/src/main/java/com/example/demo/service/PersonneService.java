package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.Personne;
import com.example.demo.repository.AdresseRepository;
import com.example.demo.repository.PersonneRepository;

import lombok.AllArgsConstructor;

// @Service = @Component
@Service
//Remplace l'autowired recommandé par spring
@AllArgsConstructor
public class PersonneService implements GenericService{

    private PersonneRepository personneRepository;
    private AdresseRepository adresseRepository;

    public List<Personne> findAll(){
        return personneRepository.findAll();
    }

    public Personne findById (int id) {
        return personneRepository.findById(id).orElse(null);
    }

    public Personne save (Personne personne) {
        adresseRepository.saveAll(personne.getAdresses());
        return personneRepository.save(personne);
    }

    public void deleteById (int id) {
        personneRepository.deleteById(id);
    }

    public Personne update (Personne personne) {
        return personneRepository.save(personne);
    }
}
