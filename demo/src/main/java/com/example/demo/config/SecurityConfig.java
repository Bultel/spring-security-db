package com.example.demo.config;

import org.springframework.boot.autoconfigure.info.ProjectInfoProperties.Build;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import lombok.AllArgsConstructor;

// class de config @Configuration
// Active le web secu, permet de modifier les comptes utilisateurs @EnableWebSecurity
// Active les anotation de securité @EnableMethodSecurity(securedEnabled = true)
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
@AllArgsConstructor // tout les constructeurs
public class SecurityConfig {

    // utilisateur en memoire, sans BD
    // Definition des roles
    // #############################Donnée en dur###########################
    //@Bean
    // InMemoryUserDetailsManager users(){
    //     return  new InMemoryUserDetailsManager(
    //         // Definition des roles
    //         User.builder().username("admin").password("{noop}admin").roles("ADMIN", "USER").build(),  //noop = no op password encoder pas de cryptage
    //         User.builder().username("user").password("{noop}user").roles("USER").build()
    //     );
    // }

    // Definie les droits des roles
    private UserDetailsService userDetailsService;

    //UNIQUEMENT POUR TEST
    // @Bean
    // NoOpPasswordEncoder encoder(){
    //     return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    // }

    @Bean
    PasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    AuthenticationManager authenticationManager(HttpSecurity httpSecurity) throws Exception{
        return httpSecurity.getSharedObject(AuthenticationManagerBuilder.class)
            .userDetailsService(userDetailsService)
            .passwordEncoder(encoder())
            .and()
            .build();
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
        return http
        .csrf().disable() // le csrf JUSTE pour les tests avec Postman, à enlever en prod
            .authorizeHttpRequests(auth -> auth
                // .requestMatchers(HttpMethod.GET, "/personnes").hasRole("USER")
                // .requestMatchers(HttpMethod.POST, "/personnes").hasRole("ADMIN")
                .anyRequest().authenticated()
            )
            // Auth basic
            .httpBasic(Customizer.withDefaults())
            .build();
    }
}
