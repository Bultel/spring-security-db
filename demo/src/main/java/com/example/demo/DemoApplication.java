package com.example.demo;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.example.demo.model.Adresse;
import com.example.demo.model.Personne;
import com.example.demo.model.Role;
import com.example.demo.model.User;

import com.example.demo.repository.UserRepository;
import com.example.demo.service.PersonneService;

import lombok.AllArgsConstructor;

@SpringBootApplication
@AllArgsConstructor
public class DemoApplication {

    private PasswordEncoder encoder;
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

    

	@Bean    
	CommandLineRunner start(PersonneService personneService, UserRepository userRepository) {
        return args -> {
            personneService.save(new Personne("Wick", "John", List.of(new Adresse("paradis", "13006", "Marseille"))));
            personneService.save(new Personne("Dalton", "Jack", List.of(new Adresse("plantes", "75014", "pARIS"))));

            userRepository.save(new User("user", encoder.encode("user"), List.of(new Role("USER")))); 
            userRepository.save(new User("admin", encoder.encode("admin"), List.of(new Role("ADMIN")))); 
        
        };
    }

}
